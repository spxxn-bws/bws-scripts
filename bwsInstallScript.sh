#!/bin/bash
mkdir /root/bws
cd /root/bws
wget https://github.com/bitwarden/sdk/releases/download/bws-v0.4.0/bws-x86_64-unknown-linux-gnu-0.4.0.zip
unzip ./bws-x86_64-unknown-linux-gnu-0.4.0.zip
cp ./bws /bin